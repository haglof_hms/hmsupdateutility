#pragma once

#include "resource.h"

// This DLL for ADO DBhandling; 061031 p�d
#import "msado15.dll" \
no_namespace rename("EOF", "EndOfFile")

/////////////////////////////////////////////////////////////////////////
// CDBBaseClass_ADODirect
class CDBBaseClass_ADODirect
{
	//private:
	_ConnectionPtr m_pConnection;
	_CommandPtr m_pCommand;
	_RecordsetPtr m_pRecSet;

	CString m_sErrorMessage;
	HRESULT hr;
	void setExceptionMsg(_com_error &e);

	BOOL isEOF(void);
public:
	CDBBaseClass_ADODirect(void);
	~CDBBaseClass_ADODirect(void);

	CString getErrorMessage(void);

	BOOL setupConnectionToDatabase(CString);

	void setDefaultDatabase(CString);
	_bstr_t getDefaultDatabase(void);

	BOOL connectionExecute(CString);

	BOOL commandExecute(CString);
	BOOL recordsetQuery(CString);
	void recordsetClose(void);

	virtual int getInt(CString);
};


/////////////////////////////////////////////////////////////////////////
// CSQLServer_ADODirect
class CSQLServer_ADODirect : public CDBBaseClass_ADODirect
{
public:
	CSQLServer_ADODirect(void);

	BOOL connectToDatabase(int authentication,
												 CString user_id,
												 CString psw,
												 CString data_source,	// e.g. PADDATOR\\SQLEXPRESS
												 CString init_catalog = _T(""));	// e.g. gallbas
	BOOL existsDatabase(CString db_name);
	BOOL existsTableInDB(CString db_name,CString table_name);
	BOOL dropTableInDB(CString db_name,CString table_name);

};


#define REG_ROOT										("SOFTWARE\\HaglofManagmentSystem\\Database")
#define REG_AUTHENTICATION_ITEM			("IsWindowsAuthentication")	// Set a key on if Windows Authentication  = 0 or Server Authentication = 1

#define REG_STR_DBLOCATION					("DBLocation")
#define REG_STR_DBSERVER						("DBServer")
#define REG_STR_SELDB								("DBSelected")
#define REG_STR_USERNAME						("UserName")
#define REG_STR_PSW									("Password")
// Name of the database holding tabels for administration; 060217 p�d
#define ADMIN_DB_NAME								("hms_administrator")

//////////////////////////////////////////////////////////////////////////////////////////
// Setup SQL Scripts; 080708 p�d

#define FstPropertyTable						"fst_property_table"
#define alter_FstPropertyTable			"alter table fst_property_table add obj_id varchar(30) NULL"


#define EstiTraktTable							"esti_trakt_table"
#define alter_EstiTraktTable				"alter table esti_trakt_table add trakt_wside tinyint NULL"
