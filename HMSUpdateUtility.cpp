// HMSUpdateUtility.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "HMSUpdateUtility.h"

#include <conio.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Setup a list of Provider strings, depending on Database to connect to; 061031 p�d

// Define string variables for connection
_bstr_t strConSQLServer_Windows_Authentication("Provider=SQLOLEDB;Initial Catalog=%s;Data Source=%s;Trusted_Connection=yes;");
_bstr_t strConSQLServer_Server_Authentication("Provider=SQLOLEDB;Persist Security Info=False;User ID=%s;Password=%s;Initial Catalog=%s;Data Source=%s;");

_bstr_t strConACCESS("Driver={Microsoft Access Driver (*.mdb)};Dbq=%s;Uid=;Pwd=;");


/////////////////////////////////////////////////////////////////////////
// CDBBaseClass_ADODirect

// Contructor
CDBBaseClass_ADODirect::CDBBaseClass_ADODirect(void)
{
	//	Initialize the COM Library
	CoInitialize(NULL);

	m_pConnection = _ConnectionPtr("ADODB.Connection");
	m_pCommand = _CommandPtr("ADODB.Command");
	m_pRecSet = _RecordsetPtr("ADODB.Recordset");

}

// Destructor
CDBBaseClass_ADODirect::~CDBBaseClass_ADODirect(void)
{
	m_pConnection->Close();
	m_pConnection = NULL;
	m_pCommand = NULL;
	m_pRecSet = NULL;

	// Uninitialize the COM Library
	CoUninitialize();
}

// PRIVATE
void CDBBaseClass_ADODirect::setExceptionMsg(_com_error &e)
{
	CString sErrMsg;
	TCHAR tmp[127];
	_bstr_t bstrSource(e.Source());
	_bstr_t bstrDescription(e.Description());
	sErrMsg.Format(_T("\n Source : %s \n Description : %s \n"),bstrSource,bstrDescription);
	m_sErrorMessage = tmp;
}

BOOL CDBBaseClass_ADODirect::isEOF(void)
{
	return (m_pRecSet->EndOfFile || m_pRecSet->BOF);
}

// PUBLIC

CString CDBBaseClass_ADODirect::getErrorMessage(void)
{
	return m_sErrorMessage;
}

BOOL CDBBaseClass_ADODirect::setupConnectionToDatabase(CString connection_str)
{

	BOOL bReturn = FALSE;
	try
	{
		//	Open the connection, based on connection string
		hr = m_pConnection->Open(bstr_t(connection_str),"","",0);
		if(FAILED(hr))
		{
			AfxMessageBox(_T("Error Opening Database object\n"));
		}
		else
		{
			// Setup connection
			m_pCommand->ActiveConnection = m_pConnection;
		}
		bReturn = TRUE;
	}
	catch(_com_error &)
	{
		bReturn = FALSE;
	}

	return bReturn;
}

void CDBBaseClass_ADODirect::setDefaultDatabase(CString db_name)
{
	m_pConnection->DefaultDatabase = _bstr_t(db_name);
}

_bstr_t CDBBaseClass_ADODirect::getDefaultDatabase(void)
{
	return m_pConnection->DefaultDatabase;
}

// Handle connection execute. Use this command for INSERT,UPDATE and DELETE; 061102 p�d
BOOL CDBBaseClass_ADODirect::connectionExecute(CString sql_query)
{
	return (m_pConnection->Execute(bstr_t(sql_query),NULL,adExecuteNoRecords) != NULL);
}

// Handle command execute; 061102 p�d
BOOL CDBBaseClass_ADODirect::commandExecute(CString command_str)
{
	m_pCommand->CommandText = _bstr_t(command_str); 
	return (m_pCommand->Execute(NULL,NULL,adExecuteNoRecords || adCmdText) != NULL);

}

BOOL CDBBaseClass_ADODirect::recordsetQuery(CString cmd)
{
	return (m_pRecSet->Open(bstr_t(cmd), 
								  _variant_t((IDispatch *)m_pConnection,true), 
								 adOpenStatic, adLockOptimistic, adCmdText) == S_OK);
}

void CDBBaseClass_ADODirect::recordsetClose(void)
{
	m_pRecSet->Close();
}

int CDBBaseClass_ADODirect::getInt(CString fld_name)
{
	_variant_t varValue = m_pRecSet->GetCollect(_bstr_t(fld_name));
	if (varValue.vt == VT_I1 ||
		 varValue.vt == VT_I2 ||
		 varValue.vt == VT_I4 ||
		 varValue.vt == VT_I8 ||
		 varValue.vt == VT_INT)
	{
		return varValue.intVal;
	}
	else
		return -1;
}

/////////////////////////////////////////////////////////////////////////
// CSQLServer_ADODirect

CSQLServer_ADODirect::CSQLServer_ADODirect(void)
			: CDBBaseClass_ADODirect()
{
}

BOOL CSQLServer_ADODirect::connectToDatabase(int authentication,
																						 CString user_id,
																						 CString psw,
																						 CString data_source,	// e.g. PADDATOR\\SQLEXPRESS
																						 CString init_catalog)	// e.g. gallbas
{
	CString sConnectionStr;
	// Check authentication; 080208 p�d
	if (authentication == 0) // Windows authentication
	{
		// Setup Connection string; 061101 p�d
		sConnectionStr.Format(strConSQLServer_Windows_Authentication,init_catalog,data_source);
	}
	else if (authentication == 1)	// Server authentication
	{
		// Setup Connection string; 061101 p�d
		sConnectionStr.Format(strConSQLServer_Server_Authentication,user_id,psw,init_catalog,data_source);
	}


	return CDBBaseClass_ADODirect::setupConnectionToDatabase(sConnectionStr);
}

BOOL CSQLServer_ADODirect::existsDatabase(CString db_name)
{
	BOOL bReturn = FALSE;
	CString sSQL;
	sSQL.Format(_T("select DB_ID('%s') as 'dbid'"),db_name);
	if (recordsetQuery(sSQL))
	{
		bReturn = (getInt(_T("dbid")) > 0);
		recordsetClose();
	}
	return bReturn;
}

BOOL CSQLServer_ADODirect::existsTableInDB(CString db_name,CString table_name)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	// Gaet name of database already set; 061101 p�d
	CString sDatabaseSet = getDefaultDatabase();
	// Set SQL question to send to database db_name; 061101 p�d
	sSQL.Format(_T("select object_id('%s','U') as 'objectid'"),table_name);
	// Set Default database to db_name; 061101 p�d
	setDefaultDatabase(db_name);
	if (recordsetQuery(sSQL))
	{
		bReturn = (getInt(_T("objectid")) > 0);
		recordsetClose();
	}
	// Get back to previously set database; 061101 p�d
	setDefaultDatabase(sDatabaseSet);

	return bReturn;
}

BOOL CSQLServer_ADODirect::dropTableInDB(CString db_name,CString table_name)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	// Gaet name of database already set; 061101 p�d
	CString sDatabaseSet = getDefaultDatabase();
	// Set SQL question to send to database db_name; 061101 p�d
	sSQL.Format(_T("drop table %s;"),table_name);
	// Set Default database to db_name; 061101 p�d
	setDefaultDatabase(db_name);
	bReturn = connectionExecute(sSQL);
	// Get back to previously set database; 061101 p�d
	setDefaultDatabase(sDatabaseSet);

	return bReturn;
}

CString regGetStr(CString key,CString item)
{
	DWORD dwDisp;
	HKEY hk;
	TCHAR szValue[512];
	DWORD dwKeySize = 255;
	wcscpy_s(szValue,_T(""));
	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
  if (RegCreateKeyEx(HKEY_CURRENT_USER,
								key,
								0,
								NULL,
								REG_OPTION_NON_VOLATILE,
								KEY_READ,
								NULL,
								&hk,
								&dwDisp) == ERROR_SUCCESS)
	{

		// Retrieve the value of the Left key
		RegQueryValueEx(hk,
									item,
									0,
									NULL,//&dwType,
									(PBYTE)&szValue,
									&dwKeySize);
	}
	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);

	return szValue;
}

BOOL GetRegistryDBData(CString& location,CString& db_server,CString& db_selected,CString& user,CString& psw,int *idx)
{
	CString sAuthenticaction;

	location = regGetStr(CString(REG_ROOT),CString(REG_STR_DBLOCATION));
	db_server = regGetStr(CString(REG_ROOT),CString(REG_STR_DBSERVER));
	db_selected = regGetStr(CString(REG_ROOT),CString(REG_STR_SELDB));
	user = regGetStr(CString(REG_ROOT),CString(REG_STR_USERNAME));
	psw = regGetStr(CString(REG_ROOT),CString(REG_STR_PSW));
	sAuthenticaction = regGetStr(CString(REG_ROOT),CString(REG_AUTHENTICATION_ITEM));
	*idx = atoi(bstr_t(sAuthenticaction));

	return TRUE;
}

// Create database table from string; 080627 p�d
BOOL runSQLScriptFileEx(CString table_name,CString script)
{
	CString sUserName;
	CString sDBServer;
	CString sDBSelected;
	CString sPSW;
	CString sLocation;
	int nAuthentication;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString sScript;

	try
	{

		if (!script.IsEmpty())
		{
			if (GetRegistryDBData(sLocation,sDBServer,sDBSelected,sUserName,sPSW,&nAuthentication))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (sLocation.Compare(_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (!sLocation.IsEmpty() &&
									 !sDBSelected.IsEmpty() &&
									 !sUserName.IsEmpty() &&
									 !sPSW.IsEmpty());
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						_tprintf(_T("Connected to database : %s\nAlter table : %s\n"),sDBSelected,table_name);
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBSelected))
							{
								if (pDB->existsTableInDB(sDBSelected,table_name))
								{
									pDB->setDefaultDatabase(sDBSelected);
									sScript.Format(script,table_name);
									pDB->commandExecute(sScript);
									bReturn = TRUE;
								}	// if (pDB->existsTableInDB(sDBSelected,table_name))
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (strcmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		BSTR sMsg = e.Description();

		_tprintf(_T("\n\n*************\n"));
		_tprintf(_T("ERROR:\n%s\n"),sMsg);
		_tprintf(_T("*************\n\n"));
		bReturn = FALSE;
	}
	
	return bReturn;
}


CString getRes(int id)
{
	CString sRes;
	sRes.LoadStringW(id);

	return sRes;
}


void ShowCommandsOnHelp(void)
{
	_tprintf(_T("Help ...\n\n"));
	_tprintf(_T("-? Show this help\n"));
	_tprintf(_T("-a1 Alter table fst_property_table\n"));
	_tprintf(_T("-a2 Alter table esti_trakt_table\n"));
}

BOOL DoAlterTable(int idx)
{
	_tprintf(_T("Alter DataBaseTables ...\n\n"));
//	Function for running "hardcoded" SQL-scripts; 080708 p�d
	switch (idx)
	{
		case 1 : return runSQLScriptFileEx(CString(FstPropertyTable),CString(alter_FstPropertyTable));
		case 2 : return runSQLScriptFileEx(CString(EstiTraktTable),CString(alter_EstiTraktTable));
	};

}


// The one and only application object

CWinApp theApp;

using namespace std;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;
	CString sCmd;
	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: MFC initialization failed\n"));
		nRetCode = 1;
	}
	else
	{
		_tprintf(_T("HMS Update Utility\n\n"));
		if (argc == 1)
		{
			ShowCommandsOnHelp();
		}	// if (argc == 1)
		// Check if there's any arguments, i.e. argc > 1; 080708 p�d

		if (argc == 2)
		{
			sCmd = CString(argv[1]);
			//-----------------------------------------------------------
			//	Argument: -? Show helptext
			//-----------------------------------------------------------
			if (sCmd.Compare(_T("-?")) == 0)
			{
				ShowCommandsOnHelp();
			}	// if (sCmd.Compare(_T("-?")) == 0)
			//-----------------------------------------------------------
			//	Argument: -u Alter table, based on "hardcoded" script(s); 080708 p�d
			//-----------------------------------------------------------
			if (sCmd.Compare(_T("-a1")) == 0)
			{
				if (!DoAlterTable(1))
					nRetCode = -1;

			}	// if (sCmd.Compare(_T("-a")) == 0)
			else if (sCmd.Compare(_T("-a2")) == 0)
			{
				if (!DoAlterTable(2))
					nRetCode = -1;

			}	// if (sCmd.Compare(_T("-a")) == 0)
		}	// if (argc == 2)
	}

	if (nRetCode == -1)
		_tprintf(_T("\n\nHMS Update Completed with errors!\n\n"));
	else
		_tprintf(_T("\n\nHMS Update Completed OK!\n\n"));
	
	_getch();

	return nRetCode;
}
